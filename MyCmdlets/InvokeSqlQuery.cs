﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Data.SqlClient;

namespace MyCmdlets
{
    [Cmdlet(VerbsLifecycle.Invoke, "SqlQuery", DefaultParameterSetName=IntegratedAuth)]
    public class InvokeSqlQuery : PSCmdlet
    {
        private const string IntegratedAuth = "IntegratedAuth";
        private const string SqlAuth = "SqlAuth";

        [Parameter(Position = 1, ParameterSetName = IntegratedAuth)]
        [Parameter(Position = 1, ParameterSetName = SqlAuth)]
        public string Server { get; set; }

        [Parameter(Position = 2, ParameterSetName = IntegratedAuth)]
        [Parameter(Position = 2, ParameterSetName = SqlAuth)]
        public string Database { get; set; }

        [Parameter(Position = 3, ParameterSetName = IntegratedAuth)]
        [Parameter(Position = 3, ParameterSetName = SqlAuth)]
        public string Query { get; set; }

        [Parameter(Position = 4, ParameterSetName = SqlAuth, Mandatory=true)]
        public string Username { get; set; }

        [Parameter(Position = 5, ParameterSetName = SqlAuth, Mandatory = true)]
        public string Password { get; set; }

        private SqlConnection _conn;

        protected override void ProcessRecord()
        {
            using (var cmd = _conn.CreateCommand())
            {
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = Query;

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var record = new PSObject();

                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            record.Properties.Add(
                                new PSVariableProperty(
                                    new PSVariable(reader.GetName(i), reader[i])));  //name, value
                        }

                        WriteObject(record);
                    }
                }
            }
        }

        protected override void BeginProcessing()
        {
            ValidateParameters();

            WriteVerbose(this.ParameterSetName);

            string connectionString;

            // Cmdlet sigures out which ParameterSet is called, we just check it and use appropriate connString
            if (this.ParameterSetName == IntegratedAuth)
            {
                connectionString = String.Format(
                    @"Data Source=.\{0};Initial Catalog={1};Persist Security Info=True;Integrated Security=SSPI",
                    Server, Database);

            }
            else
            {
                connectionString = String.Format(
                    @"Data Source=.\{0};Initial Catalog={1};User ID={2};Password={3}",
                    Server, Database, Username, Password);

            }
            
            _conn = new SqlConnection(connectionString);
            _conn.Open();
        }

        protected override void EndProcessing()
        {
            if (_conn != null)
                _conn.Dispose();
        }

        protected override void StopProcessing()
        {
            if (_conn != null)
                _conn.Dispose();
        }

        private void ValidateParameters()
        {
            const string serverVariable = "InvokeSqlQueryServer";
            const string databaseVariable = "InvokeSqlQueryDatabase";
            
            // if Server parameter is supplied, set it as a SessionState variable
            if (!String.IsNullOrEmpty(Server)){
                SessionState.PSVariable.Set(serverVariable, Server);
            }
            else
            {
                // Get value of serverVariable from SessionState
                Server = SessionState.PSVariable.GetValue(serverVariable, String.Empty).ToString();

                if (String.IsNullOrEmpty(Server))
                    ThrowParameterError("Server");
            }

            if (!String.IsNullOrEmpty(Database))
            {
                SessionState.PSVariable.Set(databaseVariable, Database);
            }
            else
            {
                Database = SessionState.PSVariable.GetValue(databaseVariable, String.Empty).ToString();
                if (String.IsNullOrEmpty(Database))
                    ThrowParameterError("Database");
            }
        }

        private void ThrowParameterError(string parameterName)
        {
            ThrowTerminatingError(
                new ErrorRecord(
                    new ArgumentException(String.Format(
                        "Must specify '{0}'", parameterName)),
                        Guid.NewGuid().ToString(),
                        ErrorCategory.InvalidArgument,
                        null));
        }

    }
}
