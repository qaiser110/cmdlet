﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.IO;
using System.Reflection;

namespace MyCmdlets
{
    [Cmdlet(VerbsCommon.Get, "RandomName")]
    public class GetRandomName : Cmdlet
    {
        [Parameter(Position=1, Mandatory=true, ValueFromPipeline=true)]
        public string Name { get; set; }

        protected override void ProcessRecord()
        {
            //Console.WriteLine("{0} (Len {1})", Name, Name.Length);
/*
            var nameCharacters = Name.ToCharArray();
            Array.Reverse(nameCharacters);

            WriteObject(new
            {
                ReversedName = new String(nameCharacters),
                NameLength = Name.Length
            });
*/ 
            WriteObject(
                _names.Where(n => n.Length == Name.Length)
                    .OrderBy(n => Guid.NewGuid())
                    .First()
                );


        }

        protected override void BeginProcessing()
        {
            WriteVerbose("Processing Begun");
            WriteVerbose("Loading names.txt file ");

            _names = File.ReadAllLines(
                Path.Combine(
                // GetExecutingAssembly will get the Debug folder path (MyCmdlets\bin\Debug)
                    Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                    "names.txt"
                )
            );
        }

        protected override void EndProcessing()
        {
            
        }

        protected override void StopProcessing()
        {
            
        }

        private string[] _names;

        //private static readonly string[] _names = new[]
        //{
        //    "Aalia", "Abigail", "Bessie", "Betty", "Brad", "Bud", "Casie", "Cam", "Doogy", "Daniel", "Ema", "Eve",
        //    "Aalia1", "Abigail1", "Bessie1", "Betty1", "Brad1", "Bud1", "Casie1", "Cam1", "Doogy1", "Daniel1", "Ema1", "Eve1"
        //};

    }
}
